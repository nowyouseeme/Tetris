package com.mochizuki.tetris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Disposable;
import com.mochizuki.tetris.Bricks.AbsBrick;

public class Board extends Actor implements Disposable {

	public static final int COLUMNS_COUNT = 12;
	public static final int LINES_COUNT = 21;

	private int[][] m_Matrix;

	private float m_piceWidth;
	private float m_piceHeight;
	private Texture m_Pices;
	private TextureRegion[] m_Pice;

	public Board(float picewidth, float piceheight) {
		// TODO Auto-generated constructor stub

		m_Matrix = new int[LINES_COUNT][COLUMNS_COUNT];
		for (int i = 0; i < LINES_COUNT; i++) {
			m_Matrix[i][0] = 9;
			m_Matrix[i][COLUMNS_COUNT - 1] = 9;
		}
		for (int i = 0; i < COLUMNS_COUNT; i++) {
			m_Matrix[LINES_COUNT - 1][i] = 9;
		}
		m_piceWidth = picewidth;// / (COLUMNS_COUNT-2);
		m_piceHeight = piceheight;// / (LINES_COUNT-1);
		m_Pices = new Texture(Gdx.files.internal("bricks/cube.png"));
		m_Pice = new TextureRegion[7];
		for (int i = 0; i < 7; i++)
			m_Pice[i] = new TextureRegion(m_Pices, 18 * i, 0, 17, 17);
		setWidth(picewidth * (COLUMNS_COUNT - 2));
		setHeight(piceheight * (LINES_COUNT - 1));
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		m_Pices.dispose();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		// super.draw(batch, parentAlpha);
		int piceindex;

		for (int y = 0; y < LINES_COUNT - 1; y++) {
			for (int x = 1; x < COLUMNS_COUNT - 1; x++) {
				if (m_Matrix[y][x] > 0 && m_Matrix[y][x] <= 7) {
					piceindex = m_Matrix[y][x] - 1;
					// else
					// piceindex = 0;
					batch.draw(m_Pice[piceindex], (x - 1) * m_piceWidth
							+ getX(), (LINES_COUNT - 1 - y) * m_piceHeight
							+ getY(), m_piceWidth, m_piceHeight);
				}
			}
		}

	}

	@Override
	public void setX(float x) {
		// TODO Auto-generated method stub
		super.setX(x);
	}

	@Override
	public void setY(float y) {
		// TODO Auto-generated method stub
		super.setY(y);
	}

	public boolean canDown(AbsBrick tetris) {
		int[][] t = tetris.getCurrentMatrix();
		int line, column;
		for (column = 0; column < 4; column++) {
			line = 3;
			while (line >= 0 && t[line][column] == 0) {
				line--;
			}
			if (line >= 0
					&& (m_Matrix[tetris.getCoordLine() + line + 1][tetris
							.getCoordColumn() + column] > 0))
				return false;
		}
		return true;
	}

	public boolean canMoveLeft(AbsBrick tetris) {
		int[][] t = tetris.getCurrentMatrix();
		int line, column;
		for (line = 0; line < 4; line++) {
			column = 0;
			while (column <= 3 && t[line][column] == 0) {
				column++;
			}
			if (column <= 3
					&& m_Matrix[tetris.getCoordLine() + line][tetris
							.getCoordColumn() + column - 1] > 0)
				return false;

		}
		return true;

	}

	public boolean canMoveRight(AbsBrick tetris) {
		int[][] t = tetris.getCurrentMatrix();
		int line, column;
		for (line = 0; line < 4; line++) {
			column = 3;
			while (column >= 0 && t[line][column] == 0) {
				column--;

			}
			if (column >= 0
					&& m_Matrix[tetris.getCoordLine() + line][tetris
							.getCoordColumn() + column + 1] > 0)
				return false;

		}
		return true;

	}

	class CanRotationResult {
		boolean canRotation;
		int offsetLeft;
		int offsetRight;
		int offsetBottom;
	}

	public CanRotationResult canRotation(AbsBrick tetris, int direction) {
		CanRotationResult res = new CanRotationResult();
		res.offsetBottom = 0;
		res.offsetLeft = 0;
		res.offsetRight = 0;
		int[][] t = tetris.getNextMatrix(direction);

		int mostleft = tetris.mostLeft(t);
		int mostright = tetris.mostRight(t);
		int mostbottom = tetris.mostBottom(t);
		// Log.i(this.getClass().getSimpleName(), String.format(
		// "Most Left=%1$d,Most Right=%2$d,Most Bottom=%3$d,Column=%4$d",
		// mostleft, mostright, mostbottom, tetris.getCoordColumn()));
		if (tetris.getCoordColumn() + mostleft < 1)
			res.offsetLeft = 1 - (tetris.getCoordColumn() + mostleft);

		if (tetris.getCoordColumn() + mostright > COLUMNS_COUNT - 2)
			res.offsetRight = tetris.getCoordColumn() + mostright
					- (COLUMNS_COUNT - 2);

		if (tetris.getCoordLine() + mostbottom > LINES_COUNT - 2)
			res.offsetBottom = tetris.getCoordLine() + mostbottom
					- (COLUMNS_COUNT - 2);

		// Log.i(this.getClass().getSimpleName(), String.format(
		// "Offset Left=%1$d,Offset Right=%2$d,Offset Bottom=%3$d",
		// res.offsetLeft, res.offsetRight, res.offsetBottom));

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (/*
					 * tetris.getCoordLine() + i >= 0 && tetris.getCoordLine() +
					 * i <= LINES_COUNT - 1 && tetris.getCoordColumn() + j >= 0
					 * && tetris.getCoordColumn() + j <= COLUMNS_COUNT - 1 &&
					 */t[i][j] > 0
						&& m_Matrix[tetris.getCoordLine() + i][tetris
								.getCoordColumn()
								+ j
								+ res.offsetLeft
								- res.offsetRight] > 0) {
					res.canRotation = false;
					return res;

				}
			}
		}
		res.canRotation = true;
		return res;

	}

	/**
	 * 把方块中的数据拷贝到面板矩阵中
	 * 
	 * @param tetris
	 */
	public void copyTetris(AbsBrick tetris) {
		int[][] t = tetris.getCurrentMatrix();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (t[i][j] > 0)
					m_Matrix[tetris.getCoordLine() + i][tetris.getCoordColumn()
							+ j] = t[i][j];
			}
		}
	}

	/**
	 * 取得每个小单元的宽
	 * 
	 * @return
	 */
	public float getPiceWidth() {
		return m_piceWidth;

	}

	/**
	 * 取得每个小单元的高
	 * 
	 * @return
	 */
	public float getPiceHeight() {
		return m_piceHeight;
	}

	/**
	 * 清除填满的行，并返回清除的行数
	 * 
	 * @return
	 */
	public int clearLines() {
		int line = LINES_COUNT - 2;
		int count = 0;
		while (line >= 0) {
			int column = 1;
			while (column <= COLUMNS_COUNT - 2 && m_Matrix[line][column] > 0) {
				column++;
			}
			if (column > COLUMNS_COUNT - 2) {
				for (int i = 1; i < COLUMNS_COUNT - 1; i++)
					m_Matrix[line][i] = 0;
				moveMatrix(line - 1);
				count++;
				continue;
			}
			line--;
		}
		return count;
	}

	/*
	 * 将startline起始的数据整体下移一行
	 * 
	 * @param startline
	 */
	private void moveMatrix(int startline) {
		int line, column;
		for (line = startline; line >= 0; line--) {
			for (column = 1; column < COLUMNS_COUNT - 1; column++) {
				m_Matrix[line + 1][column] = m_Matrix[line][column];
			}
		}
	}

	/**
	 * 清除所有数据
	 */
	public void clearAll() {

		for (int y = 0; y < LINES_COUNT - 1; y++) {
			for (int x = 1; x < COLUMNS_COUNT - 1; x++) {
				m_Matrix[y][x] = 0;
			}
		}
	}

	/**
	 * 读取面板数据，以便保存数据
	 * 
	 * @return
	 */
	public String getBoardData() {
		// int[] r = new int[LINES_COUNT * COLUMNS_COUNT];
		String r = "";
		int line, column;
		// int i = 0;
		for (line = 0; line < LINES_COUNT; line++) {
			for (column = 0; column < COLUMNS_COUNT; column++) {
				r += String.valueOf(m_Matrix[line][column]);
				// i++;
			}
		}
		return r;
	}

	/**
	 * 设置数据，将读取的数据恢复
	 * 
	 * @param d
	 */
	public void setBoardData(String d) {
		int len = d.length();
		if (len < COLUMNS_COUNT * LINES_COUNT)
			return;
		int line = 0, column = 0;
		for (int i = 0; i < len; i++) {
			if (column >= COLUMNS_COUNT) {
				column = 0;
				line++;
			}
			m_Matrix[line][column] = Integer.valueOf(d.substring(i, i+1));
			column++;
		}
	}

	public int getBoardDataCount() {
		return LINES_COUNT * COLUMNS_COUNT;
	}
}
